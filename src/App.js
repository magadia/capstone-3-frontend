import React from 'react';
import {
  HashRouter,
  Route,
  Switch
} from 'react-router-dom';

const loading = () => {
 return (
  <div className="bg-warning d-flex justify-content-center align-items-center">
    <h1>Loading...</h1>
  </div>
  )
}
const Register = React.lazy(()=>import('./views/Pages/Register'));
const Login = React.lazy(()=>import('./views/Pages/Login'));
const Rooms = React.lazy(()=>import('./views/Room/Rooms'));
const Home = React.lazy(()=>import('./views/Layout/Home'))

const Booking = React.lazy(()=>import('./views/Booking/Bookings'))


const App = () => {
  return (
    <HashRouter>
      <React.Suspense fallback={loading()}>
        <Switch>
          <Route 
            path="/register"
            exact
            name="Register"
            render={props => <Register {...props} />}
          />
          <Route 
            path="/login"
            exact
            name="Login"
            render={props => <Login {...props}/>}
          />
          <Route 
            path="/rooms"
            exact
            name="Rooms"
            render={props=> <Rooms 
                {...props}
              />}
          />
          <Route 
            path="/"
            exact
            name="Home"
            render={props=><Home />}
          />
         
          <Route 
            path="/book"
            exact
            name="Bookings"
            render={props=><Booking {...props}/>}
          />
        
        </Switch>
      </React.Suspense>
    </HashRouter>
  )
}

export default App;

