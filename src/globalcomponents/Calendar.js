import React from 'react';
import moment from 'moment';
import Helmet from 'react-helmet';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';
import { formatDate, parseDate } from 'react-day-picker/moment';
export default class Calendar extends React.Component {
  constructor(props) {
    super(props);
    this.handleFromChange = this.handleFromChange.bind(this);
    this.handleToChange = this.handleToChange.bind(this);
    this.checkInvalidDates = this.checkInvalidDates.bind(this);
    this.state = {
      startDate: undefined,
      endDate: undefined,
    };
  }
  showFromMonth() {
    const { startDate, endDate } = this.state;
    if (!startDate) {
      return;
    }
    if (moment(endDate).diff(moment(startDate), 'months') < 2) {
      this.endDate.getDayPicker().showMonth(startDate);
    }
  }
  handleFromChange(startDate) {
    this.setState({startDate });
    this.props.handleStartDateChange(startDate);
  }
  handleToChange(endDate) {
    this.setState({endDate}, this.showFromMonth);
    this.props.handleEndDateChange(endDate);
  }
     checkInvalidDates(date) {

        return this.bookedOutDates.includes(date.format('Y/MM/DD')) || date.diff(moment(), 'days') < 0;
    }
  render() {
    const { startDate, endDate } = this.state;
    const modifiers = { start: startDate, end: endDate };
    return (
      <div className="InputFromTo py-3">
        <DayPickerInput
          value={startDate}
          placeholder="Start Date"
          format="LL"
          formatDate={formatDate}
          parseDate={parseDate}
          dayPickerProps={{
            selectedDays: [startDate, { startDate, endDate }],
            disabledDays: [{ before: new Date()}, {after: endDate}],
            toMonth: endDate,
            modifiers,
            numberOfMonths: 2,
            onDayClick: () => this.endDate.getInput().focus(),
          }}
          onDayChange={this.handleFromChange}
        />{' '}
        —{' '}
        <span className="InputFromTo-to">
          <DayPickerInput
            ref={el => (this.endDate = el)}
            value={endDate}
            placeholder="End Date"
            format="LL"
            formatDate={formatDate}
            parseDate={parseDate}
            dayPickerProps={{
              selectedDays: [startDate, { startDate, endDate }],
              disabledDays: [{ before: new Date()}, {before: startDate}],
              modifiers,
              month: startDate,
              fromMonth: startDate,
              numberOfMonths: 2,
            }}
            onDayChange={this.handleToChange}
          />
        </span>
        <Helmet>
          <style>{`
            .InputFromTo .DayPicker-Day--selected:not(.DayPicker-Day--start):not(.DayPicker-Day--end):not(.DayPicker-Day--outside) {
              background-color: #F0F8FF !important;
              color: #4A90E2;
            }
            .InputFromTo .DayPicker-Day {
              border-radius: 0 !important;
            }
            .InputFromTo .DayPicker-Day--start {
              border-top-left-radius: 50% !important;
              border-bottom-left-radius: 50% !important;
            }
            .InputFromTo .DayPicker-Day--end {
              border-top-right-radius: 50% !important;
              border-bottom-right-radius: 50% !important;
            }
            .InputFromTo .DayPickerInput-Overlay {
              width: 550px;
            }
            .InputFromTo-to .DayPickerInput-Overlay {
              margin-left: -198px;
            }
          `}</style>
        </Helmet>
      </div>
    );
  }
}