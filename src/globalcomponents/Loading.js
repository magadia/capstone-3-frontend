import React from 'react';

const Loading = () => {
  return (
    <div
      className="vh-100 vw-100 d-flex align-items-center justify-content-center bg-secondary">
      <h1>Loading...</h1>
    </div>
  )
}

export default Loading;