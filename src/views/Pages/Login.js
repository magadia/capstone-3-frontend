import React, {useState} from 'react';
import {FormInput} from '../../globalcomponents'
import {Button} from 'reactstrap'
import axios from 'axios'

const Login = () => {

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	const handleEmailChange = e => {
		setEmail(e.target.value);
	}

	const handlePasswordChange = e => {
		setPassword(e.target.value);
	}

	const handleLogin = () => {
		axios.post('https://cryptic-beyond-49893.herokuapp.com/login',{
			email,
			password
		}).then(res=>{
			sessionStorage.token = res.data.token;
			sessionStorage.user = JSON.stringify(res.data.user)
			console.log(res.data.token)

			window.location.replace('/')
		})
	}

	return (
		<React.Fragment>
		<div className="logDiv">
		<br /><br />
			<h1 className="text-center py-5">Login</h1>
			<div
				className="col-lg-4 offset-lg-4"
			>
			<FormInput 
				label={"Email"}
				placeholder={"Enter your email"}
				type={"email"}
				onChange={handleEmailChange}
			/>
			<FormInput 
				label={"Password"}
				placeholder={"Enter your password"}
				type={"password"}
				onChange={handlePasswordChange}
			/>
			<Button
				block
				color="info"
				onClick={handleLogin}
			>
				Login
			</Button>
			</div>
		</div>
			
		</React.Fragment>
		)
}

export default Login;