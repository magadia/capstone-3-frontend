import React, {useState} from 'react';
import {FormInput} from '../../globalcomponents';
import {
	Button
} from 'reactstrap';
import axios from 'axios';
import {
	Link
} from 'react-router-dom'


const Register = () => {


	const [name, setName] = useState("");
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	const handleNameChange = (e) => {
		setName(e.target.value)
		console.log(e.target.value)
	}

	const handleEmailChange = (e) => {
		setEmail(e.target.value);
		console.log(e.target.value)
	}

	const handlePasswordChange = e => {
		setPassword(e.target.value);
		console.log(e.target.value);
	}

	const handleRegister = () => {
		axios.post("https://cryptic-beyond-49893.herokuapp.com/register", {
			name: name,
			email: email,
			password: password
		}).then(res=>{console.log(res.data)
			window.location.replace("#/login")}
		)
	}

	return (

		<React.Fragment>
		
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
			  <a class="navbar-brand" href="#"><img className="imgReg"src="https://images.pexels.com/photos/2625766/pexels-photo-2625766.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" /> Ski Chalet Booking</a>
			  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
			    <span class="navbar-toggler-icon"></span>
			  </button>

			  <div class="collapse navbar-collapse" id="navbarColor01">
			    <ul class="navbar-nav mr-auto">
			      <li class="nav-item active navReg">
			        <Link to="#"
			        className="text-white"
			        >Gallery</Link>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			      </li>
			      <li class="nav-item active">
			        <Link to="#"
			        className="text-white"
			        >Discounts/Promo's</Link>
			      </li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			      <li class="nav-item active">
			        <Link to="/login"
			        className="text-white"
			        >Login</Link>
			      </li>
			    </ul>
			    
			  </div>
			</nav>

			<div className="regDiv1">
			<div className="col-lg-4 offset-lg-7">
			<br /> <br /><br /><br />
			<div className="card cardReg border-primary" className="">
                <div className="card-header cardheaderReg text-center">
                Register
                </div>
	                <div className="card-body cardbodyReg">
						<FormInput 
							label={"Name"}
							placeholder={"Enter your name"}
							type={"text"}
							onChange={handleNameChange}
						/>
						<FormInput 
							label={"Email"}
							placeholder={"Enter your email"}
							type={"email"}
							onChange={handleEmailChange}
						/>
						<FormInput 
							label={"Password"}
							placeholder={"Enter your password"}
							type={"password"}
							onChange={handlePasswordChange}
						/>
						<Button
							block
							color="info"
							onClick={handleRegister}
						>
							Register
						</Button>
					</div>
				</div>
			</div>
		</div>
		<div className="regDiv2">
			<div className="container">
			<h1 className="text-center py-5">Our Branches</h1>
				<div className="card-deck text-center">
		        <div className="card">
		          <img src="https://media.cntraveler.com/photos/549966eddf8f55bf04225ec0/master/w_2048,h_1536,c_limit/val-disere-france-cr-hemis-alamy.jpg" className="cardImg" alt="..." />
		          <div className="card-body">
		            <h5 className="card-title">Baguio, Philippines</h5>
		          </div>
		        </div>
		        <div className="card">
		          <img src="http://cdn.cnn.com/cnnnext/dam/assets/160105164129-05-ski-resorts-zermatt.jpg" className="cardImg" alt="..." />
		          <div className="card-body">
		            <h5 className="card-title">Zermatt, Switzerland</h5>
		          </div>
		        </div>
		        <div className="card">
		          <img src="https://blog.holidaylettings.co.uk/wp-content/uploads/2017/05/80-1.jpg" className="cardImg" alt="..." />
		          <div className="card-body">
		            <h5 className="card-title">Courchevel, France</h5>
		          </div>
		        </div> 
		        </div><br />
		        <div className="card-deck text-center">
		        <div className="card">
		          <img src="https://image.jimcdn.com/app/cms/image/transf/none/path/sa6549607c78f5c11/image/i9aa167138e094327/version/1541510270/avoriaz-best-ski-resorts-europe.jpg" className="cardImg" alt="..." />
		          <div className="card-body">
		            <h5 className="card-title">Arlberg, Austria</h5>
		          </div>
		          </div>
		        <div className="card">
		          <img src="https://qtxasset.com/styles/breakpoint_sm_default_480px_w/s3/2016-12/franceski.jpg?Gtk_6tBJmMuD7SlMZjWiC0v.d0zH.Mp.&itok=xTdD8rVu" className="cardImg" alt="..." />
		          <div className="card-body">
		            <h5 className="card-title">Lech, Austria</h5>
		          </div>
		          </div>
		      </div>
			</div>
		</div>
			
		</React.Fragment>

	)
}

export default Register;
