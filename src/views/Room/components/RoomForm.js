import React from 'react';
import {
	Modal,
	ModalHeader,
	ModalBody,
	Button
} from 'reactstrap';
import {FormInput} from '../../../globalcomponents'

const RoomForm = props => {
	return (
		<Modal
			isOpen={props.showForm}
			toggle={props.handleShowForm}
		>
			<ModalHeader
				toggle={props.handleShowForm}
				style={{"backgroundColor":"	#2F4F4F", "color": "white"}}
			>
			New Chalet
			</ModalHeader>
			<ModalBody>
				
				<FormInput
					label={"Chalet Name"}
					type={"text"}
					name={"name"}
					placeholder={"Enter Name"}
					required={props.nameRequired}
					onChange={props.handleRoomNameChange}
				/>
				<FormInput
					label={"Description"}
					type={"text"}
					name={"description"}
					placeholder={"Enter Description"}
					required={props.descriptionRequired}
					onChange={props.handleRoomDescriptionChange}
				/>

				<input
	                className = "form-control"
	                type = "file"
	                accept="image/*"
	                onChange = {(e)=>props.handleRoomImageChange(e)}
                    />
                <br />
				<FormInput
					label={"Can accommodate./person"}
					name={"occupancy"}
					type={"number"}
					placeholder={"How many?"}
					required={props.occupancyRequired}
					onChange={props.handleRoomOccupancyChange}
				/>
				<FormInput
					label={"Accommodation price/night"}
					type={"number"}
					name={"price"}
					placeholder={"Enter price"}
					required={props.priceRequired}
					onChange={props.handleRoomPriceChange}
				/>
		
				<Button
					color="success"
					onClick={props.handleSaveRoom}
					disabled={props.name!="" && props.description != "" && props.occupancy != "" && props.occupancy > 0 ? false : true && props.price != "" && props.price >0 ? false : true}
					style={{width:"468px"}}
				>Submit</Button>
			</ModalBody>
		</Modal>
	)
}

export default RoomForm;