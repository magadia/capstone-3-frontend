import React, {useState, useEffect} from 'react';
import {Elements, StripeProvider} from 'react-stripe-elements';
import CheckoutForm from './CheckoutForm';
import Swal from 'sweetalert2'
import { Modal, ModalHeader, ModalBody, Button,ModalFooter} from "reactstrap";
import {FormInput,Calendar} from '../../../globalcomponents';
import axios from 'axios';
import moment from 'moment';

const BookForm = props => {

  const user = props.user;
  const Swal = require('sweetalert2')
  const [modal, setModal] = useState(false);
  const [nestedModal, setNestedModal] = useState(false);
  const [closeAll, setCloseAll] = useState(false);
  const [bookings, setBookings] = useState([]);
  const [toPay, setToPay] = useState(0);
  const [toDay, setDays] = useState(0);
  const [toStart, setStart] = useState("");
  const [toEnd, setEnd] = useState("");
  const [startDate, setStartDate] = useState("");
  const [endDate, setEndDate] = useState("");

  const goBook = ()=> {     
    handleSaveBook()    
    toggleNested()
  }

  const handleStartDateChange = (startDate) => {
    setStartDate(startDate);
    }
  const handleEndDateChange = (endDate) => {
    setEndDate(endDate);
    }

    const toggle = () => setModal(!modal);

  const toggleNested = () => 
   {
      setNestedModal(!nestedModal);
      setCloseAll(false);
   }

    const toggleAll = () => 
    {
      setNestedModal(!nestedModal);
      setCloseAll(true);
    }

      const handleSaveBook = ()=>{
        let startDates = moment(startDate).format("MMMM DD YYYY")
        let endDates = moment(endDate).format("MMMM DD YYYY")
        let bookingCode = moment(new Date).format("x");
        let roomName = props.room.name
        let roomId = props.room.rooomId
        let days = Math.floor((endDate - startDate) / (1000*60*60*24))
        let user = props.user.name
        let userId = props.user.id
        let totalPayment = props.room.price * days
        setToPay(totalPayment);
        setDays(days);
        setStart(startDates);
        setEnd(endDates);

      axios.post('https://cryptic-beyond-49893.herokuapp.com/addbooking',{
        roomName: roomName,
        roomId: roomId,
        startDate: startDates,
        endDate: endDates,
        days: days,
        user: user,
        userId: userId,
        totalPayment: totalPayment,
        bookingCode: bookingCode



      }).then(res=>{
        let newBookings = [...bookings]
        newBookings.push(res.data)
        setBookings(newBookings)
      })
    }
  
return (
    <Modal
      isOpen={props.showBookForm}
      toggle={props.handleShowBookForm}
    >
      <ModalHeader
        toggle={props.handleShowBookForm}
        style={{"backgroundColor":" #2F4F4F", "color": "white"}}
      >
      Book Now
      </ModalHeader>
      <ModalBody className="text-center">
      <h5 className="text-center py-3">Choose your desire date!</h5>
        <Calendar
        handleStartDateChange={handleStartDateChange}
        handleEndDateChange={handleEndDateChange}
        />

        <Button
          color="info"
          onClick={goBook}
        >Book and Pay Now
        </Button>

        <br />
        <hr/>
        <h4>{props.room.name}</h4>
        <h6>{props.room.description}</h6>
        <h6>Can accommodate up to: {props.room.occupancy} persons</h6>
        <h5>Pay only: ₱ {props.room.price} /person</h5>

        <Modal 
        isOpen={nestedModal} 
        toggle={toggleNested} 
        onClosed={closeAll ? toggle : undefined}
        >
              <ModalFooter>
                <Button color="primary" 
                onClick={toggleNested}>
                Done
                </Button>
                {' '}
                <Modal 
                isOpen={nestedModal} 
                toggle={toggleNested} 
                onClosed={closeAll ? toggle : undefined}>

              <ModalHeader>
              Payment via Stripe
              </ModalHeader>
                <ModalBody>
                  <StripeProvider apiKey="pk_test_CFBxW3zX8xIiqAZ1kl01Dyfw00scbYGhmf">
              <div>
                <div className="d-flex justify-content-center">
                  <Elements>
                    <CheckoutForm 
                    toPay={toPay}
                    toDay={toDay}
                    />
                  </Elements>
                </div>
              </div>
            </StripeProvider>

                </ModalBody>
            <ModalFooter>
              <Button 
              color="success" 
              style={{width:"500px"}}
              onClick={toggleNested}>
              Done
              </Button>{' '}
      
            </ModalFooter>
          </Modal>
              </ModalFooter>
            </Modal>
          </ModalBody>
        
          
        </Modal>
    
    
  )
}

export default BookForm;