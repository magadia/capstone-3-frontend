import React from 'react';
import {CardElement, injectStripe} from 'react-stripe-elements';
import axios from 'axios';
import Swal from 'sweetalert2'

const CheckoutForm = (props) => {

	const Swal = require('sweetalert2')
	const submit = async (e) => {
		let user = JSON.parse(sessionStorage.user);
		console.log(user.email);
		console.log(props.toPay);
		let {token} = await props.stripe.createToken({name:"Name"})

		axios.post('https://cryptic-beyond-49893.herokuapp.com/charge',{
			email: user.email,
			amount: (props.toPay * 100)

		}).then(Swal.fire({
			position: 'top',
			title: 'Confirm Payment',
			showCancelButton: true,
			confirmButtonColor: '#228B22',
			cancelButtonColor: '#FF0000',
			confirmButtonText: 'Proceed'
		}).then((result) => {
			if (result.value) {
				Swal.fire(
					'Transaction Done',
					'Payment successfully executed'
					)
			}
		}));
	}
	return (
		<div className="checkout">
			<h6>Number of Days: {props.toDay}</h6>
			<h6>You have to pay: {props.toPay}</h6>
        	<p>Please enter your Card Number</p>
        	<CardElement />
        	<button className="btn btn-primary my-3" onClick={submit}>Submit</button>
      	</div>
	)
}
export default injectStripe(CheckoutForm);