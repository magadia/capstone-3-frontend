import React, {useState} from 'react';
import BookForm from './BookForm';
import axios from 'axios';
import {
	Button, 
	Col, 
	Card, 
	CardBody, 
	CardTitle, 
	CardSubtitle, 
	CardText,
	Row,
	CardImg,
	Container
} from 'reactstrap'
import {
FormInput
} from '../../../globalcomponents';

const RoomList = props => {

	const [showBookForm, setShowBookForm] = useState(false);
	const user = props.user;
	const room = props.room;
	const handleShowBookForm = () => {
		setShowBookForm(!showBookForm)	
	}
	return (
		<React.Fragment>
		<div className="container-fluid">	
		<div className="card mb-4">
	        <div className="row no-gutters">
	          <div className="col-md-7">
	            <img src={'https://cryptic-beyond-49893.herokuapp.com/' + room.image} className="card-img" style={{height:"55vh", width:"90vh"}}  />
	          </div>
	          <div className="col-md-5">
	            <div className="card-body">
	            <br />
					<h3
					onClick={()=>props.handleNameEditInput(room._id)}

					>{props.showName && props.editId === room._id
				? 
				<FormInput
					defaultValue={room.name}
					type={"text"}
					onBlur={(e)=>props.handleEditName(e, room._id)}
				/>
				: room.name}</h3>
					<h6
					onClick={()=>props.handleDescriptionEditInput(room._id)}

					>{props.showDescription && props.editId === room._id
				? 
				<FormInput
					defaultValue={room.description}
					type={"text"}
					onBlur={(e)=>props.handleEditDescription(e, room._id)}
				/>
				: room.description}</h6>

				<h6
					onClick={()=>props.handleOccupancyEditInput(room._id)}

					>Can accommodate up to: {props.showOccupancy && props.editId === room._id
						?
					 	<FormInput 
						defaultValue={room.occupancy}
						type={"number"}
						onBlur={(e)=>props.handleEditOccupancy(e, room._id)}
						/> 
					: room.occupancy
						} persons</h6>&nbsp;
					<h5
						onClick={()=>props.handlePriceEditInput(room._id)}
						>₱ {props.showPrice && props.editId === room._id
						?
					 	<FormInput 
						defaultValue={room.price}
						type={"number"}
						onBlur={(e)=>props.handleEditPrice(e, room._id)}
						/> 
					: room.price
						} per night
					</h5>
					
					<Button color="outline-secondary" onClick={handleShowBookForm} 
					className="col-lg-12"
					style={{width:"190px"}}
					>
					Start Booking
					</Button>
					&nbsp;
					{user.isAdmin
						?
					<Button
					color="outline-danger"
					onClick={()=>props.handleDeleteRoom(room._id)}
					className="col-lg-12 my-3"
					style={{width:"190px"}}
					>
					Delete
					</Button>
					:
					""
					}
					<BookForm
					showBookForm={showBookForm}
					handleShowBookForm={handleShowBookForm}
					room={room}
					user={user}
					/>
	            </div>
	          </div>
	        </div>
	    </div>
	    </div>

					
		
		</React.Fragment>
	)
}

export default RoomList;
