import React, {useEffect, useState} from 'react';
import {
	Link
} from 'react-router-dom'

const Navbar = () => {

	const [user, setUser] = useState({})

	useEffect(()=>{
		if(sessionStorage.token){
			let user = JSON.parse(sessionStorage.user)
			console.log(user);
			setUser(user);
		}else{
			window.location.replace("#/register");
		}
	}, [])

	return (
		<React.Fragment>
			
			<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
			  <a class="navbar-brand" href="#"><img className="imgReg"src="https://images.pexels.com/photos/2625766/pexels-photo-2625766.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" /> Ski Chalet Booking</a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
			    <span class="navbar-toggler-icon"></span>
			  </button>

			  <div className="collapse navbar-collapse" id="navbarColor01">
			    <ul className="navbar-nav mr-auto">
			      <li className="nav-item active">
			        <Link to="/"
			        className="text-white"
			        >Home</Link>
			      </li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

			      <li class="nav-item">
			        <Link to="#"
			        className="text-white"
			        >Gallery</Link>
			      </li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

			      <li class="nav-item">
			        <Link to="/rooms"
			        className="text-white"
			        >Ski Chalets</Link>
			      </li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

			      <li class="nav-item">
			        <Link to="/book"
			        className="text-white"
			        >Bookings</Link>
			      </li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

			      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

			      <li class="nav-item">
			        <Link to="#"
			        className="text-white"
			        ><p className="liNav">Welcome! {user.name}</p></Link>
			      </li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

			      <li class="nav-item">
			        <Link to="#"
			        className="text-white"
			        >Logout</Link>
			      </li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			    </ul>
			    
			  </div>
			</nav>

		</React.Fragment>
	)
}

export default Navbar;