import React from 'react';
import {Navbar,Footer} from '../Layout';

const Home = () => {
	return (
		<React.Fragment>
			<Navbar />
				<div className="homeDiv">
				</div>
				<div className="regDiv2">
			<div className="container">
			<h1 className="text-center py-5">Our Branches</h1>
				<div className="card-deck text-center">
		        <div className="card">
		          <img src="https://media.cntraveler.com/photos/549966eddf8f55bf04225ec0/master/w_2048,h_1536,c_limit/val-disere-france-cr-hemis-alamy.jpg" className="cardImg" alt="..." />
		          <div className="card-body">
		            <h5 className="card-title">Baguio, Philippines</h5>
		          </div>
		        </div>
		        <div className="card">
		          <img src="http://cdn.cnn.com/cnnnext/dam/assets/160105164129-05-ski-resorts-zermatt.jpg" className="cardImg" alt="..." />
		          <div className="card-body">
		            <h5 className="card-title">Zermatt, Switzerland</h5>
		          </div>
		        </div>
		        <div className="card">
		          <img src="https://blog.holidaylettings.co.uk/wp-content/uploads/2017/05/80-1.jpg" className="cardImg" alt="..." />
		          <div className="card-body">
		            <h5 className="card-title">Courchevel, France</h5>
		          </div>
		        </div> 
		        </div><br />
		        <div className="card-deck text-center">
		        <div className="card">
		          <img src="https://image.jimcdn.com/app/cms/image/transf/none/path/sa6549607c78f5c11/image/i9aa167138e094327/version/1541510270/avoriaz-best-ski-resorts-europe.jpg" className="cardImg" alt="..." />
		          <div className="card-body">
		            <h5 className="card-title">Arlberg, Austria</h5>
		          </div>
		          </div>
		        <div className="card">
		          <img src="https://qtxasset.com/styles/breakpoint_sm_default_480px_w/s3/2016-12/franceski.jpg?Gtk_6tBJmMuD7SlMZjWiC0v.d0zH.Mp.&itok=xTdD8rVu" className="cardImg" alt="..." />
		          <div className="card-body">
		            <h5 className="card-title">Lech, Austria</h5>
		          </div>
		          </div>
		      </div>
			</div>
		</div>
		<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
			
			<Footer />
		</React.Fragment>
	)
}

export default Home;