import React from 'react';

const Footer = () => {
	return (
		<div className="card-footer cardFoot text-muted text-center bg-dark bottom">
		    <h5>Credit to the owners(PICTURES). No copyright infringement is intended</h5>
		    <h6>For Capstone 3 Project purposes only. Not for sale!!</h6>
		</div>
	)
}

export default Footer