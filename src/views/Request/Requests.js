import React, {useEffect, useState} from 'react';
import axios from 'axios'
import {RequestRow, AddForm} from './components'
import {
	Button
} from 'reactstrap'
import moment from 'moment'
import {Navbar, Footer} from '../Layout'

const Requests = () => {
	const [requests, setRequests] = useState([])
	const [showForm, setShowForm] = useState(false);
	const [assetName, setAssetName] = useState("");
	const [assetId, setAssetId] = useState("");
	const [quantity, setQuantity] = useState(1);
	const [editId, setEditId] = useState("");
	const [showStatus, setShowStatus] = useState(false);
	const [showQuantity, setShowQuantity] = useState(false);
	const [user, setUser] = useState({})

	const handleRefresh = () => {
		setShowForm(false);
		setAssetName("");
		setAssetId("");
		setQuantity(1);
		setEditId("");
	}

	useEffect(()=>{

		if(sessionStorage.token){
			let user = JSON.parse(sessionStorage.user)
			if(user.isAdmin){
				axios.get('https://cryptic-beyond-49893.herokuapp.com/admin/showrequests').then(res=>{
				setRequests(res.data)
				})
			}else{
				axios.get('https://cryptic-beyond-49893.herokuapp.com/admin/showrequestsbyuser/'+user.id).then(res=>{
					setRequests(res.data)
					console.log(res.data)
				})
			}

			setUser(user);

		}else{
			window.location.replace('#/login')
		}

	}, []);

	const handleDeleteRequest = (requestId) => {
		axios.delete('https://cryptic-beyond-49893.herokuapp.com/admin/deleterequest/'+requestId).then(res=>{
			let index = requests.findIndex(request=>request._id===requestId);
			let newRequests = [...requests];
			newRequests.splice(index,1);
			setRequests(newRequests);
		})
	}

	const handleShowAddForm = () => {
		setShowForm(!showForm);
	}

	const handleAssetInput = asset => {
		setAssetName(asset.name)
		setAssetId(asset._id)
	}

	const handleQuantityInput = (e) => {
		setQuantity(e.target.value)
	}

	const handleSaveRequest = () => {
		let code = moment(new Date).format("x")
		let requestor=user.name
		let requestorId = user.id
		let approver = "CHAN"
		let approverId = ":D"

		axios.post('https://cryptic-beyond-49893.herokuapp.com/admin/addrequest',{
			code: code,
			quantity: quantity,
			assetName: assetName,
			assetId: assetId,
			requestor: requestor,
			requestorId: requestorId,
			approver: approver,
			approverId: approverId
		}).then(res=>{
			let newRequests = [...requests]
			newRequests.push(res.data)
			setRequests(newRequests)
			handleRefresh();
		})
	}

	const handleShowStatus = (editId) => {
		setEditId(editId);
		setShowStatus(true);
		setShowQuantity(false);
		// console.log(editId)
	}

	const handleShowQuantity = (editId) =>{
		setEditId(editId);
		setShowQuantity(true);
		setShowStatus(false);
	}
	const handleStatusInput = (status, logs) =>{

		let log = {
			status:"pending",
			updateDate: moment(new Date).format("MM/DD/YYYY"),
			message: "Updated Status to " + status	
		}

		axios.patch('https://cryptic-beyond-49893.herokuapp.com/admin/updaterequeststatus/'+editId,{
			status,
			logs: log
		}).then(res=>{
			let index = requests.findIndex(request=>request._id==editId);
			console.log(res.data)
			let newRequests = [...requests];
			newRequests.splice(index,1,res.data);
			setRequests(newRequests);
			handleRefresh();
		})
	}

	const handleEditQuantityInput = (e, request) => {

		let oldQuantity = request.quantity
		let quantity = e.target.value

		let log = {
			status: "pending",
			updateDate: moment(new Date).format('MM/DD/YYYY'),
			message: "Quantity updated from " + oldQuantity + " to " + quantity 
		}

		axios.patch('https://cryptic-beyond-49893.herokuapp.com/admin/updaterequestquantity/'+editId,{
			quantity,
			logs: log
		}).then(res=>{
			let index = requests.findIndex(request=>request._id==editId)
			let newRequests = [...requests]
			newRequests.splice(index,1,res.data)
			setRequests(newRequests)
			handleRefresh()
		})
	}

	return (
		<React.Fragment>
		<Navbar />
			<div>
				<div className="text-center">
				<h1 className="text-center py-5">Suit Rent</h1>
				<Button
					color="success"
					onClick={handleShowAddForm}
					className="my-3"
				>
					Start Renting
				</Button>
				<AddForm 
					showForm={showForm}
					handleShowAddForm={handleShowAddForm}
					handleAssetInput = {handleAssetInput}
					assetName={assetName}
					handleQuantityInput={handleQuantityInput}
					quantity={quantity}
					handleSaveRequest={handleSaveRequest}
				/>	
				</div>
				<div className="col-lg-10 offset-lg-1">
					<table className="table table-striped border">
						<thead>
							<tr>
								<th>Request Code</th>
								<th>Date Requested</th>
								<th>Asset Requested</th>
								<th>Requested By</th>
								<th>Book From</th>
								<th>Book To</th>
								<th>totalPayment</th>
								<th>Status</th>
								<th>Approved By</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
							{requests.map(request=>
							<RequestRow 
								key={request._id}
								request={request}
								handleDeleteRequest={handleDeleteRequest}
								handleShowStatus={handleShowStatus}
								showStatus={showStatus}
								editId={editId}
								handleStatusInput={handleStatusInput}
								handleShowQuantity={handleShowQuantity}
								showQuantity={showQuantity}
								handleEditQuantityInput={handleEditQuantityInput}
								user={user}
							/>
							)
							}
						</tbody>
					</table>
				</div>
			</div>
			<Footer/>
		</React.Fragment>
	)
}

export default Requests;