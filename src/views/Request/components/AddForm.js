import React from 'react';
import {
	Modal,
	ModalHeader,
	ModalBody,
	Button	
} from 'reactstrap';
import {FormInput} from '../../../globalcomponents';
import {AssetInput} from '../components'

const AddForm = (props) => {
	return (
		<React.Fragment>
			<Modal
				isOpen={props.showForm}
				toggle={props.handleShowAddForm}

			>
				<ModalHeader
				toggle={props.handleShowAddForm}
				style={{backgroundColor:"blue"}}
				>	
					Rent Form
				</ModalHeader>
				<ModalBody>
					<AssetInput 
						handleAssetInput={props.handleAssetInput}
						assetName={props.assetName}
					/>
					<FormInput
						name={"quantity"}
						label={"Quantity"}
						type={"number"}
						placeholder={"Input quantity"}
						onChange={props.handleQuantityInput}
						defaultValue={1}
					/>
					<Button
						color="info"
						block
						disabled={props.assetName=="" || props.quantity <= 0 ? true : false}
						onClick={props.handleSaveRequest}
					>
						Submit
					</Button>
				</ModalBody>
			</Modal>
		</React.Fragment>

	)
}

export default AddForm;