import React, {useState, useEffect} from 'react';
import axios from 'axios';
import {
	FormGroup,
	Label,
	Dropdown,
	DropdownToggle,
	DropdownMenu,
	DropdownItem
} from 'reactstrap'

const AssetInput = (props) => {

	const [dropdownOpen, setDropdownOpen] = useState(false)
	const [assets, setAssets] = useState([])

	const toggle = () => setDropdownOpen(!dropdownOpen)

	useEffect(()=>{
		axios.get('https://cryptic-beyond-49893.herokuapp.com/admin/showassets'
			).then(res=>{
			setAssets(res.data)
			console.log(res.data)
		})
	},[])

	return (
		<FormGroup>
			<Label>Asset Name:</Label>
			<Dropdown
				isOpen={dropdownOpen}
				toggle={toggle}
			>
				<DropdownToggle caret>
					{props.assetName==""
					? "Select Suit"
					: props.assetName}
				</DropdownToggle>
				<DropdownMenu>
					{assets.map(asset=>
						<DropdownItem
							key={asset._id}
							onClick={()=>props.handleAssetInput(asset)}
						>
							{asset.name}
						</DropdownItem>
					)}
				</DropdownMenu>
			</Dropdown>
		</FormGroup>
	)
}

export default AssetInput;