import React, {useEffect, useState} from 'react';
import axios from 'axios'
import {BookList} from './components'
import {
	Col,
	Row,
	Container
} from 'reactstrap'
import moment from 'moment'
import {Navbar, Footer} from '../Layout'

const Bookings = () => {
	const [bookings, setBookings] = useState([])
	const [user, setUser] = useState({})
	

	useEffect(()=>{

		if(sessionStorage.token){
			let user = JSON.parse(sessionStorage.user)
			if(user.isAdmin){
				axios.get('https://cryptic-beyond-49893.herokuapp.com/showbookings').then(res=>{
				setBookings(res.data)
				})
			}else{
				axios.get('https://cryptic-beyond-49893.herokuapp.com/'+user.id).then(res=>{
					setBookings(res.data)
					console.log(res.data)
				})
			}

			setUser(user);

		}else{
			window.location.replace('#/login')
		}

	}, []);

	const handleDeleteBooking = (bookingId) => {
		axios.delete('https://cryptic-beyond-49893.herokuapp.com/deletebooking/'+bookingId).then(res=>{
			let index = bookings.findIndex(booking=>booking._id===bookingId);
			let newBookings = [...bookings];
			newBookings.splice(index,1);
			setBookings(newBookings);
		})
	}

	return (
		<React.Fragment>
		<Navbar />
				<h1 className="text-center py-5">Booking Details</h1>
				<div className="col-sm-12">
					<table className="table table-striped border">
						<thead className="text-center">
							<tr>
								<th>Date Reserved</th>
								<th>Room Name</th>
								<th>Book From</th>
								<th>Book To</th>
								<th>Days</th>
								<th>totalPayment</th>
								<th>Customer Name</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody className="text-center">
							
						{bookings.map(booking=>(
							<BookList 
								key={booking._id}
								booking={booking}
								handleDeleteBooking={handleDeleteBooking}
							
						/>
					))}
						</tbody>
					</table>
				</div>
			
		<Footer/>
		</React.Fragment>
	)

}
export default Bookings;