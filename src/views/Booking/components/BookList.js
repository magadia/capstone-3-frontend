import React, {useState} from 'react';
import axios from 'axios';
import {
	Button, 
	Card, 
	CardBody, 
	CardTitle, 
	CardText,
	Container
} from 'reactstrap'

const BookList = props => {
	const booking = props.booking;
	return (
		<React.Fragment>
		<tr>
			<td>{booking.createdAt}</td>
			<td>{booking.roomName}</td>
			<td>{booking.startDate}</td>
			<td>{booking.endDate}</td>
			<td>{booking.days}</td>
			<td>P {booking.totalPayment}</td>
			<td>{booking.user}</td>
			<td>
				<Button
					color="outline-danger"
					className="col-lg-12"
					onClick={()=>props.handleDeleteBooking(booking._id)}
					style={{width:"100px"}}

					>
					Delete
					</Button>
			</td>
		</tr>

		&nbsp;
		</React.Fragment>
	)
}

export default BookList;